﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Maze_Escape
{
    class Player : Actor // Player inherits from ("is an") Actor
    {


        // Constants
        private const float MOVE_ACCEL = 100000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 10;

        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFramesPerSecond)
            : base(newTexture, newFrameWidth, newFrameHeight, newFramesPerSecond)
        {
            // STUFF
            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);

            PlayAnimation("walkDown");
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                PlayAnimation("walkLeft");
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                PlayAnimation("walkRight");
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                PlayAnimation("walkUp");
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                PlayAnimation("walkDown");
            }

            // Add the movement change to the velocity
            velocity += movementInput * MOVE_ACCEL * frameTime;

            // Apply drag from the ground
            velocity *= MOVE_DRAG_FACTOR;

            // If the speed is too high, clamp it to a reasonable max
            if (velocity.Length() > MAX_MOVE_SPEED)
            {
                velocity.Normalize();
                velocity *= MAX_MOVE_SPEED;
            }

            // If the player isn't moving, stop the animation
            if (velocity.Length() < MIN_ANIM_SPEED)
            {
                StopAnimation();
            }

            base.Update(gameTime);
        }
        // ------------------
        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle wallBounds = hitWall.GetBounds();

        }
        // ------------------

    }
}

