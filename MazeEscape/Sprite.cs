﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Sprite
    {
        // ------------------
        // Data
        // ------------------
        protected Vector2 position;
        protected Texture2D texture;
        protected bool visible = true;


        // ------------------
        // Behaviour
        // ------------------
        public Sprite(Texture2D newTexture)
        {
            texture = newTexture;
        }
        // ------------------
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(texture, position, Color.White);
            }
        }
        // ------------------
        public bool GetVisible()
        {
            return visible;
        }
        // ------------------
        public void SetVisible(bool newVisible)
        {
            visible = newVisible;
        }
        // ------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        // ------------------
        public virtual Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
        }
        // ------------------
    }
}

